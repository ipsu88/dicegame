﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice
{
    class Program
    {

        private static int guessedNumber;

        /// <summary>
        /// @author Ilona Puikkonen
        /// @version 12.4.2016
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            
            Dicegame dg = new Dicegame();
            dg.Running=true;
            while (dg.Running)
            {
                StartGame(dg);
            }
            
        }

        private static void StartGame(Dicegame dg)
        {
            Console.WriteLine("Arvaa kahden nopan silmälukujen summa ja heitä noppaa painamalla enter: ");
            dg.ThrowDice();
            CheckInput(dg);
            PrintResult(dg);
            dg.Running = Continue();
        }

        private static void PrintResult(Dicegame dg)
        {
            if (!dg.IsCorrect(guessedNumber))
            {
                Console.WriteLine("Heitetty summa: " + dg.ThrownNumber);
                Console.WriteLine("Väärin meni!");

            }
            else
            {
                Console.WriteLine("heitetty summa: " + dg.ThrownNumber);
                Console.WriteLine("Arvasit oikein!");

            }
        }
        private static void CheckInput(Dicegame dg)
        {
            
            if (int.TryParse(Console.ReadLine(), out guessedNumber))
            {
                
                if (dg.IsLegal(guessedNumber))
                {
                    return;
                }
        
            }

                Console.WriteLine("Anna toimiva luku");
                CheckInput(dg);
       
        }
        private static bool Continue()
        {

            Console.WriteLine("Kirjoita 'k' jos haluat jatkaa: ");
            string continuePlay = Console.ReadLine();
            if (continuePlay == "k")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
}
