﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice
{

    public class Dicegame
    {
        public int ThrownNumber { get; set; }
        public bool Running { get; set; }




        public bool IsLegal(int number)
        {
            if (number >= 2 && number <= 12)
                return true;
            else
                return false;
        }

        public void ThrowDice()
        {
            Random r = new Random();
            ThrownNumber = r.Next(1, 7) + r.Next(1, 7);

        }

        public bool IsCorrect(int a)
        {
            if (a == ThrownNumber)
                return true;
            else
                return false;
        }

    }
}

