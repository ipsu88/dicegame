﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dice.Tests
{
    [TestClass()]
    public class DicegameTests
    {
        Dicegame testgame = new Dicegame();
        [TestMethod()]
        public void IsLegalTest()
        {
            

            Assert.AreEqual(testgame.IsLegal(54), false);
            Assert.AreEqual(testgame.IsLegal(5), true);
            Assert.AreEqual(testgame.IsLegal(2), true);
            Assert.AreEqual(testgame.IsLegal(10), true);
            Assert.AreEqual(testgame.IsLegal(1), false);
            Assert.AreEqual(testgame.IsLegal(13), false);
            Assert.AreEqual(testgame.IsLegal(7), true);

        }

     
    }
}